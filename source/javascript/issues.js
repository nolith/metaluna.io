function getObjects(group, type, assignee) {
    // type = merge_requests or issues
    // group = 9970 org 6543 com
   
    var groupid = undefined;
    if (group == 'gitlab-org') {
        groupid = 9970;
    } else if (group == 'gitlab-com') {
        groupid = 6543;
    };

    var token = localStorage.getItem("token");

    var request = new XMLHttpRequest()
    var url = "https://gitlab.com/api/v4/groups/" + groupid + "/" + type + "/?state=opened&assignee_username=" + assignee
    request.open('GET', url, true)
    request.setRequestHeader("PRIVATE-TOKEN", token)
    request.onload = function () {
        var queryurl = "https://gitlab.com/groups/" + group + "/-/" + type + "?scope=all&utf8=%E2%9C%93&state=opened&assignee_username=" + assignee
        var issuelist = "<h2>" + group + "<a href=\"" + queryurl + "\">: " + type + "</a></h2>"
        issuelist += "<ul>"
        var data = JSON.parse(request.response)

        if (data.length == 0) {
            issuelist += "<li> None"
        } else {
            data.forEach((issue) => {
                issuelist += "<li><a href=\"" + issue.web_url + "\">" + issue.title + "</a>"
            })
        }
        issuelist += "</ul>"
        document.getElementById("root").innerHTML += issuelist
    }
    request.send()
}

var user = 'jyavorska'
getObjects('gitlab-org', 'issues', user);
getObjects('gitlab-org', 'merge_requests', user);
getObjects('gitlab-com', 'issues', user);
getObjects('gitlab-com', 'merge_requests', user);
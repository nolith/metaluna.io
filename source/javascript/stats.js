/* global Chart fetch */

var ctx = document.getElementById('myChart').getContext('2d')

function drawGraph (jsonfile) {
  // Find all available pages
  var pages = Object.keys(jsonfile)

  // Find all available dates
  var dates = {}
  Object.keys(jsonfile).forEach(function (key) {
    Object.keys(jsonfile[key]).forEach(function (myDate) {
      dates[myDate] = true
    })
  })
  dates = Object.keys(dates).sort()

  var datasets = []

  // Find all available series
  pages.forEach(function (page) {
    if (page.includes("-")) {
      var dataset = []
      dataset.data = []
      dataset.label = page
      dataset.fill = 'false'
      dataset.lineTension = 0.2
      dates.forEach(function (myDate) {
        if (jsonfile[page][myDate] !== undefined) {
          dataset.data.push(jsonfile[page][myDate])
        } else {
          dataset.data.push(0)
        };
      })
      datasets.push(dataset)
    }
  })

  var config = {
    type: 'line',
    data: {
      labels: dates,
      datasets: datasets
    },
    options: {
      plugins: {
        colorschemes: {
          scheme: 'tableau.Tableau20'
        }
      }
    }
  }

  var chart = new Chart(ctx, config)
  console.log(chart)
};

fetch('/stats.json')
  .then(response => response.json())
  .then(jsonfile => drawGraph(jsonfile))

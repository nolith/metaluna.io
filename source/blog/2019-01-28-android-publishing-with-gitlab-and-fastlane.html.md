---
title:  "How to publish Android apps to the Google Play Store with GitLab and fastlane"
layout: layout
date:   2019-01-28
category: work
description: See how GitLab, together with fastlane, can build, sign, and publish apps for Android to the Google Play Store. 
canonical_url: https://about.gitlab.com/blog/2019/01/28/android-publishing-with-gitlab-and-fastlane/
---
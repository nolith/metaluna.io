---
title:  "Successful approaches for collaboration between Design, Product, Engineering, and Quality"
layout: layout
date:   2020-06-03
category: work
description: We've been focusing on improving collaboration between each of the internal teams that contribute to our product process, and identified a few key points that seem to make a big difference.
canonical_url: https://about.gitlab.com/blog/2020/06/03/collaboration-in-product-planning/
---
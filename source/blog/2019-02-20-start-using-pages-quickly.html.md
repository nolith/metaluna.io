---
title:  "How to get up and running quickly using GitLab Pages templates"
layout: layout
date:   2019-02-20
category: work
description: We're introducing bundled GitLab Pages templates, so let's take a look at how easy it really is now to get up and running with a new site.
canonical_url: https://about.gitlab.com/blog/2019/02/20/start-using-pages-quickly/
---
---
title:  "New up and coming GitLab CI/CD Features"
layout: layout
date:   2019-08-07
category: work
description: DAG, Multi-project Pipelines, Runner Setup for Kubernetes and more.
canonical_url: https://about.gitlab.com/blog/2019/08/07/a-look-ahead-for-gitlab-cicd/
---
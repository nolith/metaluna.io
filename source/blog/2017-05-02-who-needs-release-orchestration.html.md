---
title:  "Who Needs Release Orchestration and Why?"
layout: layout
date:   2017-05-02
category: work
description: IT organizations are relying more and more on using release orchestration tools instead of tracking their software releases using programs like Excel, Google Docs, and PowerPoint.
canonical_url: https://blog.xebialabs.com/2017/05/02/who-needs-release-orchestration-and-why/
---
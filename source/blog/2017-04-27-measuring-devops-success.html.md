---
title:  "Measuring DevOps Success in Your Software Delivery Pipeline"
layout: layout
date:   2017-04-27
category: work
description: When it comes to measuring the success of your DevOps rollout, it can be challenging to identify the right metrics that will provide intelligence while avoiding the trap of vanity metrics.
canonical_url: https://blog.xebialabs.com/2017/04/27/measuring-devops-success-in-your-software-delivery-pipeline/
---
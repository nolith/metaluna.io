---
title:  "How to publish iOS apps to the App Store with GitLab and fastlane"
layout: layout
date:   2019-03-06
category: work
description: See how GitLab, together with fastlane, can build, sign, and publish apps for iOS to the App Store.
canonical_url: https://about.gitlab.com/blog/2019/03/06/ios-publishing-with-gitlab-and-fastlane/
---
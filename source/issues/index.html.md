---
title: "Open GitLab Issues"
description: "Open issues on GitLab that are assigned to me."
---

This page contains the list of items currently assigned to me across the GitLab groups. It only works for me because I have a magic token set.

<div id="root"></div>
<script src="/javascript/issues.js"></script>
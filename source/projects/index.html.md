---
title: "Projects"
description: "Various projects I've built over the years."
---

### Making

- A 6502 computer based on [Ben Eater's kit](https://eater.net/6502)
- [Untitled Alien Game in Godot Engine](https://gitlab.com/personal-jyavorska/aliengame)

### Made

- [Neocities template for GitLab CI/CD](https://gitlab.com/pages/neocities)
- [JavaScript implementation of Life](https://gitlab.com/personal-jyavorska/jslife)
- [Baby monitor for Raspberry Pi](https://gitlab.com/personal-jyavorska/monitor)
- [Commodore 64 executor for GitLab Runner](https://gitlab.com/jyavorska/c64exec)
- [World Clock menubar item for MacOS](https://gitlab.com/personal-jyavorska/WorldClock)
- [Chrome plugin to show chess quote on new tab](https://gitlab.com/personal-jyavorska/newtab-lichess)
- [Source code for this website](https://gitlab.com/personal-jyavorska/metaluna.io)
- [My dotfiles](https://gitlab.com/personal-jyavorska/dotfiles)

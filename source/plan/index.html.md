---
title: "Plan"
description: "The day to day .plan for my work activities."
---

### Hacking CI/CD for GitLab

This is my .plan for work and covers what's in flight/how I work. My role at GitLab is [Director of Product Management for CI/CD](https://about.gitlab.com/job-families/product/director-of-product/).

### Logistics

* I split my day to be available in EMEA/Americas hours, my calendar automatically reflects this.
* Please differentiate between not getting to a request yet (assumed) vs not ever going to do it.
* Please @ me in issues to see it. I get too many other notifications to keep up.
* I always appreciate direct feedback, so don't worry.
* I'm more likely to be optimistic than not, feel free bring me back to reality.
* I like to debate to solve problems, if that puts you off that's fine - please tell me.
* I'm good at automating repetitive tasks so let me know how I can help with that sort of thing.

Feel free to take a look at my [most recent 360 review results](https://docs.google.com/spreadsheets/d/1GMZ95ZL2zMLUuFfwbTaLbhUR9YPmaWvc2ObS8tv5qmc/edit#gid=0) for additional context, or my [5x5 introduction slides](https://docs.google.com/presentation/d/1rWmNYIoWPJwQk8R-yyeQ7I7UeXV_2uopyyh1dKJOQ6g/edit#slide=id.p). Introduction slides are GitLab internal only for now.

### Personal Style

<img align=right width=275 height=275 style="padding-right: 20px; padding left: 20px;" src="../images/style.png">

My personal style is "expressive", which means I tend to have a creative and intuitive way of working, which works really well with iteration. It also means that I really enjoy sharing my enthusiasm and passion for the problems I am working on, and that I excel in unstructured, fast-moving environments. I'm motivated by being appreciated and working on innovative solutions. This also means that I can make mistakes or have frequent changes in direction or focus due to my inclination to act on opinions, hunches, and intuitions versus facts and data, and so I try to be careful to avoid this by working iteratively rather than via sweeping changes. A sample motto for an expressive manager like me might be "I lean on the power of my vision, and trust in my persuasive power."

Apart from this, I love coaching and developing people, and am a natural collaborator/facilitator. If there's ever anything where I can help in this regard, please just let me know.

More info can be found in the [handbook guide for the styles](https://about.gitlab.com/handbook/people-group/learning-and-development/emotional-intelligence/social-styles/#the-four-styles-and-how-to-handle).
<br clear=all>

### Overall Priorities

* 40%: Team Management/Coaching
    * This includes career development/CDF and guidance where requested from my reports, but does NOT include reviewing or being accountable for department, company, or section-level initiatives/strategies since those already have other leaders checking on them. Anywhere I'm helping the PMs deep dive/review, "unstuck" their issues, complete [product discovery](https://about.gitlab.com/handbook/product-development-flow/index.html), in all cases using this as a teaching opportunity.
* 30%: Sensing Mechanisms/Prototyping/[Research Issues](https://gitlab.com/gitlab-org/gitlab/-/issues/214655)
    * Looking out for the big picture of CI/CD and helping to ensure the organization more broadly understands what that longer term cross-stage vision is and where they fit (this may include my own PMs, which then blends a bit into the 40% above.) This also includes strong partnerships with teams like TAMs and UX that are working on the big picture.
* 15%: Product Leadership Team Engagement / Upleveling PM Org
    * This is largely supporting Product Ops by being an advocate for the GitLab culture that has made us successful, but does not include where this duplicates department, company, or section-level initiatives.
* 10%: External Evangelism and Interaction
    * Things like CI/CD Office Hours, supporting DevRel initiatives, engagement on Twitter/HN/other places, early, open-ended customer discussions (CAB), and improving our [CI/CD enablement materials for Sales & CS](https://about.gitlab.com/handbook/marketing/product-marketing/usecase-gtm/ci/). Interactions on in-progress, nearly complete, or just delivered features is owned by the DRI PM, not me.
* 5%: Keeping up on Product
    * Largely achieved through [walk-throughs](https://about.gitlab.com/handbook/product/communication/#walk-through) and [speed runs](https://about.gitlab.com/handbook/product/communication/#speed-run) of our own and competitor products.

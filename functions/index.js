const functions = require('firebase-functions')
const admin = require('firebase-admin')
const app = admin.initializeApp()

var today = new Date()
var dd = String(today.getDate()).padStart(2, '0')
var mm = String(today.getMonth() + 1).padStart(2, '0') // January is 0!
var yyyy = today.getFullYear()
today = yyyy + '_' + mm + '_' + dd

exports.counterFunction = functions.https.onRequest((request, response) => {
  response.status(204).send()
  if (!request.get('referer')) {
    return Promise.reject(new Error('no referer'))
  }
  var page = request.get('referer')
  // remove trailing slash, then everything up to the last slash, remove any dots, finally get rid of dothtml
  page = page.replace(/\/$/, '').replace(/.*\//g, '').replace(/\.html/g, '').replace(/\./g, '_')
  if (page === 'metaluna_io') {
    page = 'home'
  };
  if (page === 'localhost:5000') {
    return Promise.reject(new Error('Not logging localhost'))
  };
  if (page === 'us-central1-metalunaio_cloudfunctions_net') {
    return Promise.reject(new Error('Not logging us-central1-metalunaio_cloudfunctions_net'))
  };
  return app.database().ref(page + '/' + today).once('value', (snap) => {
    const value = snap.val() || 0
    return app.database().ref(page + '/' + today).set(value + 1).then(res => {
      return true
    }).catch(err => {
      return Promise.reject(err)
    })
  })
})
